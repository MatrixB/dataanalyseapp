# web_app/app.py
"""
Author: Muaaz Bdear
Date: 13.10.2023
Description: This script creates a Flask web application to display data from an Excel file in an HTML table.
Users can access the data interactively through a web interface.
"""
from flask import Flask, render_template
import pandas as pd

from config.config import EXCEL_FILE_PATH

app = Flask(__name__, static_folder='static')


def load_data(file_path):
    """
    Load data from an Excel file and return it as a pandas DataFrame.

    Args:
        file_path (str): Path to the Excel file.

    Returns:
        pd.DataFrame: Loaded data as a DataFrame.
    """
    try:
        df = pd.read_excel(file_path, engine='openpyxl')
        return df
    except Exception as e:
        print(f"Error loading data from {file_path}: {str(e)}")
        return None


@app.route('/')
def display_data():
    data = load_data(EXCEL_FILE_PATH)

    data_html = data.to_html(classes='table table-bordered table-striped', escape=False, index=False)
    return render_template('data.html', data=data_html)


if __name__ == '__main__':
    app.run(debug=True)
