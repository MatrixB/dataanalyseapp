# data_visualization/correlation_visualization.py
"""
Author: Muaaz Bdear
Date: 13.10.2023
Description: This script uses Seaborn and Matplotlib to visualize correlations
within numerical columns from an Excel dataset. It creates a heatmap representing correlation coefficients,
which are categorized from "+ + +" to "- - -."
"""

import seaborn as sns
import matplotlib.pyplot as plt


def classify_correlation(corr_value):
    if corr_value >= 0.75:
        return "+ + +"
    elif corr_value >= 0.5:
        return "+ +"
    elif corr_value >= 0.3:
        return "+"
    elif corr_value <= -0.75:
        return "- - -"
    elif corr_value <= -0.5:
        return "- -"
    elif corr_value <= -0.3:
        return "- -"
    else:
        return "No Correlation"


def plot_correlation_heatmap(correlation_matrix):
    """
    Create and display a correlation heatmap using Seaborn.

    Args:
        correlation_matrix (pd.DataFrame): Correlation matrix of numerical columns.
    """
    plt.figure(figsize=(10, 8))
    sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f")
    plt.title("Correlation Heatmap of Numerical Columns")
    plt.show()


if __name__ == "__main__":
    import pandas as pd

    data = pd.read_excel('../sensors_alex_roofbox_23-10-11.xlsx', engine='openpyxl')

    numerical_data = data.select_dtypes(include=['number'])
    correlation = numerical_data.corr()

    plot_correlation_heatmap(correlation)
