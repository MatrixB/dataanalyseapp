# data_visualization/data_histograms.py
"""
Author: Muaaz Bdear
Date: 13.10.2023
Description: This script uses Seaborn and Matplotlib to create and display
histograms for numerical columns in an input DataFrame. It's a tool for visualizing the distribution of data,
making it easy to understand the underlying patterns.
"""


import seaborn as sns
import matplotlib.pyplot as plt


def plot_histograms(data, numerical_columns):
    """
    Create and display histograms for numerical columns in the data.

    Args:
        data (pd.DataFrame): Input DataFrame.
        numerical_columns (list): List of numerical column names to plot histograms for.
    """
    for column in numerical_columns:
        plt.figure(figsize=(10, 6))
        sns.histplot(data[column], kde=True)
        plt.title(f'Histogram for {column}')
        plt.xlabel(column)
        plt.ylabel('Count')
        plt.show()


if __name__ == "__main__":
    import pandas as pd

    data = pd.read_excel('../sensors_alex_roofbox_23-10-11.xlsx', engine='openpyxl')

    numerical_columns = data.select_dtypes(include=['number']).columns

    plot_histograms(data, numerical_columns)
