# main.py
"""
Author: Muaaz Bdear
Date: 13.10.2023
Description: This script loads and processes data from an Excel file,
creates various visualizations including correlation matrices, histograms,
and data statistics. It generates a PDF report containing these visualizations
and serves a web application for interactive data exploration.
"""
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns

from config.config import EXCEL_FILE_PATH
from data_processing.data_loader import load_data
from data_processing.data_preprocessing import handle_duplicates, handle_missing_values, \
    remove_columns_with_fixed_values
from data_visualization.correlation_visualization import classify_correlation
from web_app.app import app
from tabulate import tabulate
from PIL import Image
from datetime import datetime


def main():
    data = load_data(EXCEL_FILE_PATH )

    if data is not None:
        # Data preprocessing
        data = handle_duplicates(data)
        data = handle_missing_values(data)
        data = remove_columns_with_fixed_values(data)

        # Extract numerical columns for correlation matrix
        numerical_columns = data.select_dtypes(include=['number']).columns
        correlation_matrix = data[numerical_columns].corr()

        # Create a PDF to save the visualizations and information
        from matplotlib.backends.backend_pdf import PdfPages
        pdf_pages = PdfPages('data_visualizations.pdf')

        # Deckblatt
        img_path = 'MicrosoftTeams-image.png'
        img = Image.open(img_path)
        plt.figure(figsize=(8.27, 11.69))
        plt.title("Data Analysis Documentation")
        current_date = datetime.now().strftime("%d.%m.%Y")
        plt.text(0.5, 0.5, f"{current_date}\n", ha='center', va='center', fontsize=20)
        plt.imshow(img)
        plt.axis('off')
        pdf_pages.savefig()

        # Classified Correlation Relationships
        classified_corr = correlation_matrix.apply(lambda x: x.map(classify_correlation))
        classified_corr_table = tabulate(classified_corr, headers='keys', tablefmt='pipe', showindex=True)
        plt.figure(figsize=(48, 8))
        plt.title("Classified Correlation Relationships")
        plt.axis('off')
        plt.text(-0.15, 0.05, classified_corr_table, fontsize=9, family='monospace')
        pdf_pages.savefig()

        # Correlation Matrix Table
        correlation_table = tabulate(correlation_matrix, headers='keys', tablefmt='pipe', showindex=True)
        plt.figure(figsize=(40, 8))
        plt.title("Correlation Matrix Table")
        plt.axis('off')
        plt.text(-0.15, 0.05, correlation_table, fontsize=10, family='monospace')
        pdf_pages.savefig()

        min_values = data[numerical_columns].min()
        max_values = data[numerical_columns].max()
        median_values = data[numerical_columns].median()
        mean_values = data[numerical_columns].mean()

        # Create a DataFrame to hold min and max values
        min_max_info = pd.DataFrame(
            {'Min': min_values, 'Mean': mean_values, 'Max': max_values, 'Median': median_values})

        # Min, Max, Median, and Mean Values
        plt.figure(figsize=(8.27, 11.69))  # A4 size
        plt.title("Min, Max, Median, and Mean Values for Numerical Columns")
        plt.axis('off')
        min_max_table = tabulate(min_max_info, headers='keys', tablefmt='pipe', showindex=True)
        plt.text(-0.08, 0.05, min_max_table, fontsize=10, family='monospace')
        pdf_pages.savefig()

        # Data visualization
        plt.figure(figsize=(20, 18))
        sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f")
        plt.title("Correlation Heatmap of Numerical Columns")
        pdf_pages.savefig()

        for column in numerical_columns:
            plt.figure(figsize=(10, 6))
            sns.histplot(data[column], kde=True)
            plt.title(f'Histogram for {column}')
            plt.xlabel(column)
            plt.ylabel('Count')
            pdf_pages.savefig()

        pdf_pages.close()

        app.run(debug=True)


if __name__ == "__main__":
    main()
