# data_processing/data_loader.py
"""
Author: Muaaz Bdear
Date: 13.10.2023
Description: This file contains a function for loading data from an Excel file and returning it as a pandas DataFrame.
"""

import pandas as pd


def load_data(file_path):
    """
    Load data from an Excel file and return it as a pandas DataFrame.

    Args:
        file_path (str): Path to the Excel file.

    Returns:
        pd.DataFrame: Loaded data as a DataFrame.
    """
    try:
        df = pd.read_excel(file_path, engine='openpyxl')
        return df
    except Exception as e:
        print(f"Error loading data from {file_path}: {str(e)}")
        return None


if __name__ == "__main__":
    file_path = '../sensors_alex_roofbox_23-10-11.xlsx'
    data = load_data(file_path)
    if data is not None:
        print("Data loaded successfully.")
