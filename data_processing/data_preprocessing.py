# data_processing/data_preprocessing.py
"""
Author: Muaaz Bdear
Date: 13.10.2023
Description: This file contains functions for data preprocessing,
including handling duplicates, missing values,
and removing columns with fixed values.
"""
import pandas as pd

from data_processing.data_loader import load_data


def handle_duplicates(df):
    """
    Check for and handle duplicates in the DataFrame.

    Args:
        df (pd.DataFrame): Input DataFrame.

    Returns:
        pd.DataFrame: DataFrame with duplicates removed.
    """
    duplicates = df[df.duplicated()]
    if not duplicates.empty:
        print("There are duplicates in the DataFrame. Duplicates have been removed.")
        df = df.drop_duplicates()
    else:
        print("There are no duplicates in the DataFrame.")
    return df


def handle_missing_values(df):
    """
    Replace NaN values in the DataFrame with 0.

    Args:
        df (pd.DataFrame): Input DataFrame.

    Returns:
        pd.DataFrame: DataFrame with NaN values replaced by 0.
    """
    df = df.fillna(0)
    return df


def remove_columns_with_fixed_values(df):
    """
    Remove columns with only one unique value from the DataFrame.

    Args:
        df (pd.DataFrame): Input DataFrame.

    Returns:
        pd.DataFrame: DataFrame with columns with fixed values removed.
    """
    unique_value_counts = df.nunique()
    columns_with_fixed_value = unique_value_counts[unique_value_counts == 1].index
    for column in columns_with_fixed_value:
        unique_value = df[column].iloc[0]
        print(f"Column: {column}, Unique Value: {unique_value}")
    df = df.drop(columns=columns_with_fixed_value)
    return df


if __name__ == "__main":
    # Example usage of data preprocessing functions
    data = pd.read_excel('sensors_alex_roofbox_23-10-11.xlsx', engine='openpyxl')

    data = handle_duplicates(data)

    data = handle_missing_values(data)

    data = remove_columns_with_fixed_values(data)

