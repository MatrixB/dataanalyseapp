# tests/test_data_visualization.py

import unittest
from data_visualization.correlation_visualization import plot_correlation_heatmap
from data_visualization.data_histograms import plot_histograms
import pandas as pd


class TestDataVisualization(unittest.TestCase):
    def setUp(self):
        self.data = pd.DataFrame({
            'A': [1, 2, 3, 4, 5],
            'B': [5, 6, 7, 8, 9],
            'C': [9, 10, 11, 12, 13]
        })

    def test_plot_correlation_heatmap(self):
        # (visualization testing)
        correlation_matrix = self.data.corr()
        plot_correlation_heatmap(correlation_matrix)

    def test_plot_histograms(self):
        # (visualization testing)
        numerical_columns = self.data.columns
        plot_histograms(self.data, numerical_columns)


if __name__ == '__main__':
    unittest.main()
