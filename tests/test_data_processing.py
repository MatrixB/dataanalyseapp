# tests/test_data_processing.py

import unittest
import pandas as pd
from data_processing.data_preprocessing import handle_missing_values, \
    remove_columns_with_fixed_values


class TestDataProcessing(unittest.TestCase):
    def setUp(self):
        self.data = pd.DataFrame({
            'A': [1, 2, 2, 3, 4],
            'B': [5, 6, 7, 8, 8],
            'C': [9, 10, 11, 12, 13]
        })

    def handle_duplicates(data):
        data_no_duplicates = data.drop_duplicates()
        return data_no_duplicates

    def test_handle_missing_values(self):
        self.data.loc[1, 'A'] = pd.NA  # Add a missing value
        data_no_missing = handle_missing_values(self.data)
        self.assertFalse(data_no_missing.isna().any().any())  # No missing values should be left

    def test_remove_columns_with_fixed_values(self):
        self.data['D'] = 0  # Add a column with a fixed value
        data_no_fixed_columns = remove_columns_with_fixed_values(self.data)
        self.assertFalse('D' in data_no_fixed_columns.columns)  # Expecting column D to be removed


if __name__ == '__main__':
    unittest.main()
